<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:15:31+01:00
 */

$GLOBALS['TL_LANG']['tl_nc_notification']['type']['avisota-subscription']         = 'Avisota-Abonnement';
$GLOBALS['TL_LANG']['tl_nc_notification']['type']['avisota_confirm_subscription'] = 'Abonnement bestätigen';
$GLOBALS['TL_LANG']['tl_nc_notification']['type']['avisota_subscribe']            = 'Anmelden';
$GLOBALS['TL_LANG']['tl_nc_notification']['type']['avisota_unsubscribe']          = 'Abmelden';
