<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:15:31+01:00
 */

$GLOBALS['TL_LANG']['tl_nc_notification']['avisotaFilterByMailingList']['0']      = 'Be trametter a glistas da mail specificas';
$GLOBALS['TL_LANG']['tl_nc_notification']['avisotaFilterByMailingList']['1']      = 'Be trametter quest avis sche ina glista da mail specifica è vegnida abunada u de-abunada.';
$GLOBALS['TL_LANG']['tl_nc_notification']['avisotaFilteredMailingLists']['0']     = 'Glistas da mail';
$GLOBALS['TL_LANG']['tl_nc_notification']['avisotaFilteredMailingLists']['1']     = 'Tscherna las glistas da mail per las qualas quest avis duai vegnir activà.';
$GLOBALS['TL_LANG']['tl_nc_notification']['avisota_legend']                       = 'Avisota';
$GLOBALS['TL_LANG']['tl_nc_notification']['type']['avisota-subscription']         = 'Abunament dad Avisota';
$GLOBALS['TL_LANG']['tl_nc_notification']['type']['avisota_confirm_subscription'] = 'Confermar l\'abunament';
$GLOBALS['TL_LANG']['tl_nc_notification']['type']['avisota_subscribe']            = 'Abunar';
$GLOBALS['TL_LANG']['tl_nc_notification']['type']['avisota_unsubscribe']          = 'De-abunar';
