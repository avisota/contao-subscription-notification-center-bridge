<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-subscription-notification-center-bridge
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Module
 */
$GLOBALS['TL_LANG']['MOD']['avisota-subscription-notification-center-bridge'] = array(
    'Avisota - Subscription NotificationCenter Bridge',
    'Bridge to the notification_center for Avisota subscriptions.'
);
