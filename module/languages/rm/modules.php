<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-13T04:00:09+01:00
 */

$GLOBALS['TL_LANG']['MOD']['avisota-subscription-notification-center-bridge']['0'] = 'Avisota - Colliaziun al center da notificaziuns davart abunaments';
$GLOBALS['TL_LANG']['MOD']['avisota-subscription-notification-center-bridge']['1'] = 'Colliaziun al center da notificaziuns davart abunaments dad Avisota.';
